# CCG Skill Building I

If you are interested in the topics presented and looking for job opportunities, please reach out to us at:
https://chameleoncg.com/#careers
contact@chameleoncg.com

![CCG Image](chameleon_color_600_dropshadow.png)

# Skill Building Series

We have been approached by developers and IT professionals over the years seeking to have guided projects to learn a learn new things. The Skill Building series is an attempt to expose the participant to essential tools and skills required to write production level software (in 2020).


# Expectations

## Development

* You may find it easiest to clone the repo, in some levels there will be scripts to help
* These levels all build on each other in some way
    * For an experienced individual, feel free to skip some of the beginning levels that hold your hand through basics
* Any language can be used to complete these challenges, although some will be considerably easier with certain languages
    * If any technology is specifically called out, it is encouraged that you explore alternatives as much as you want
    * Python is used for a specific levels, but an equivalent is possible in virtually any language

## Time to Complete

Each level is written to take between 1-4 hours to complete. This is assuming the topics are familiar to you. If it takes you longer, there is nothing wrong with that. Most likely you have learned a skill that will enable you to work quicker in the future.

## Levels

| Level | Summary
|-|-
[Build Basic Client/Server](./Level1/README.md)
[Write Automated Test](./Level1/README.md)
[Process File on Server](./Level2/README.md)
[Introduce a Message Queue](./Level3/README.md)
[Compile Module to Assembly (RISC-V)](./Level4/README.md)
[Fix The Bug](./Level5/README.md)
[Tie it all Together](./Level6/README.md)
[Expand/Explore](./Level7/README.md)

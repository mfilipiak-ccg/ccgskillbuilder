# CCG Skill Building - Level 2

## Task

Build a server that accepts a python module, and returns a response code based on the validity of the module. Our server will be used to accept a python module containing a single function that will eventually be invokable by an event.

The return codes supported are as follows:

```
 0 - module is valid and accepted
-1 - module was rejected because it was not valid python
-2 - module was rejected because it did not meet the single function requirement
-3 - module was rejected because the public function in the module collides with another already loaded function name
```

## Requirements

* Each _file_ submitted must have one and only one _public_ function defined.
    * The name of the function does not matter
    * Other functions may exist, but must be prefixed with either `_` or `__` (e.g `def _helper():`)
    * Classes and enums may exist as well
* The server will return the response 0 if and only if the input meets the above criteria
* The server must retain the processed file after a server reboot
* Although the public function name can be any valid python name, it must not collide with another _upload_ by any other client
    * The server will return -3 in this case

## Skills Learned/Required

* Processing a source file to check it's validity
* Using a database within docker, and persisting to it

## Hints

* In future levels, we will be processing the source file further. Using the python `AST` library may be beneficial at this stage, but certainly not required to meet the requirements
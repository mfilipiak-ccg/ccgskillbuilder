# CCG Skill Building - Level 1

## Task

Build TCP server that accepts the content of a python file, and returns the file with a prepended comment that reads `# Processed DATE_TIME`.

For example, if the server receives the following contents as input:

```python
if __name__ == "__main__":
    print("hello")
```

The server will respond with the following response:

```python
# Processed 8/1/2020 13:00
if __name__ == "__main__":
    print("hello")
```

## Requirements

* The `server` must be run within a container
    * The client does not need to be inside a container
* The `server` must be able to process multiple client requests at the same time
* The contents does not need to be valid python at this stage
* Automated tests must exist (in pytest or nose) to test critical components of the code
* All code must be run through pylint


## Skills Learned/Required

* Building a multi-client TCP server
* Using docker to run a python server
* Automated testing using a python based testing library
* Linting source code to find potential issues

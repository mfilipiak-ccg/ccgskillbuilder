# CCG Skill Building - Level 3

## Task

Introduce a message bus for submitting events to the server called `submitted`. When the server sees the message, it either places the processed function name and data/time on the `processed` queue, or the `rejected` queue.

An example message flow may look similar to the following (simplified for handling only fixed length method names and arguments)

```python
# function definitions, preloaded via Level 2 code
def foo(val):
    print(val)

def bar(val):
    print(val + 1)
```

```python
# > Client sends Event 1 on 1/1/2020 12:30
foo202001011230
# > Client sends Event 2 on 1/1/2020 20:15
bar1
foo202001012015
```

Consider using AMQP/RabbitMQ or Kafka for the message queue.

## Requirements

* The message format must handle variable length strings and arrays in addition to fixed length datatypes
* If the message format is invalid, send to the rejected queue (just the data/time)
* If the function or number of arguments is invalid or not found, send to the rejected queue


## Skills Learned/Required

* Using a message queue from client and server
* Dealing with binary based data
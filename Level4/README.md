# CCG Skill Building - Level 4

## Task

Modify the code used in the previous level that ingests and parses the python file. Given a simple function (that only contains +,-,*,/ math operations and '<', '>' if statements) convert it to RISC-V and return the result.

https://github.com/riscv/riscv-gnu-toolchain

## Example

```python
def score(points):
    if points < 100:
        return 50
    return points
```
May return the following:

```c
calculate_score(int):
        addi    sp,sp,-32
        sd      s0,24(sp)
        addi    s0,sp,32
        mv      a5,a0
        sw      a5,-20(s0)
        lw      a5,-20(s0)
        sext.w  a4,a5
        li      a5,99
        bgt     a4,a5,.L2
        lw      a5,-20(s0)
        j       .L3
.L2:
        lw      a5,-20(s0)
        addiw   a5,a5,-10
        sext.w  a5,a5
.L3:
        mv      a0,a5
        ld      s0,24(sp)
        addi    sp,sp,32
        jr      ra
```

## Requirements

* Parse the python file, and emit 
* Do not worry about classes, imports, other function calls, etc

The code must support uploading this module and emit correct RISC-V code.

```python
def score(points):
    if points < 100:
        return 50
    return points
```

## Skills Learned/Required

* Learning how to parse modules (hint ast module will be helpful)
* Understand the basics of assembly
* If testing the emitted assembly, understand building with new toolchains
* Most likely running on some ISA simulator/emulator